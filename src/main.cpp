#include <iostream>
#include <exception>
#include <string>
#include <vector>
#include <map>
#include <numeric>
#include <iomanip>

#include "types.hpp"
#include "csv/reader.hpp"
using namespace std;

auto read_books(map<uint32_t, vector<review_t>>& reviews) -> vector<book_t> {
    auto book_in = CSV::Reader<7>("../assets/books.csv");
    book_in.read_header("book_id", "book_title", "genre_1", "genre_2", "pages", "author_name", "author_average_rating");
    book_t book;
    vector<book_t> books;
    while (book_in.read_row(book.id, book.title, book.genre_1, book.genre_2, book.n_pages, book.author_name, book.author_ar)) {
        books.push_back(book);
        reviews.insert(pair<uint32_t, vector<review_t>>(book.id, vector<review_t>(0)));
    }
    return books;
}

auto read_reviews(map<uint32_t, vector<review_t>>& reviews) -> map<uint32_t , vector<review_t>> {
    auto review_in = CSV::Reader<3>("../assets/reviews.csv");
    review_in.read_header("book_id", "rating", "number_of_likes");
    review_t review;
//    map<uint32_t, vector<review_t>> reviews;

    while(review_in.read_row(review.book_id, review.rating, review.n_likes)) {
//        auto it = reviews.find(review.book_id);
//        if (it == reviews.end()) {
//            reviews.insert(pair<uint32_t, vector<review_t>>(review.book_id, vector<review_t>(1, review)));
//        } else {
            reviews[review.book_id].push_back(review);
//        }
    }
    return reviews;
}

auto find_best_book(vector<book_t>& books,
        map<uint32_t, vector<review_t>>& reviews,
        const string& genre_name) -> pair<book_t, double> {
    double max_book_ar = -1;
    book_t best_book;
    for (const auto& book : books) {
        if (contains(book.genre_1, genre_name) || contains(book.genre_2, genre_name)) {
            auto it = reviews.find(book.id);
            if (it != reviews.end()) {
                const auto& book_reviews = it->second;
                // calculate popularity index
                double total_book_reviews_like = 0, num = 0;
                for (auto r : book_reviews) {
                    total_book_reviews_like += r.n_likes;
                    num += r.n_likes*r.rating;
                }
                auto book_ar = (book.author_ar + num/total_book_reviews_like)/10.0;
                if (book_ar > max_book_ar) {
                    max_book_ar = book_ar;
                    best_book = book;
                }
            }
        }
    }
    return pair<book_t, double>(best_book, max_book_ar);
}

int main(int argc, char **argv) {
    try {
        /* parse commandline parameters and check them */
        if (argc < 2) {
            throw invalid_argument("please provide genre name.");
        }
        string genre_name = string(argv[1]);

        /* read csv files */
        map<uint32_t, vector<review_t>> reviews;
        auto books = read_books(reviews);
        read_reviews(reviews);

        auto result = find_best_book(books, reviews, genre_name);
        auto best_book = result.first;
        auto max_book_ar = result.second;

        cout << "id: " << best_book.id << endl;
        cout << "Title: " << best_book.title << endl;
        cout << "Genres: " << best_book.genre_1 << ", " << best_book.genre_2 << endl;
        cout << "Number of Pages: " << best_book.n_pages << endl;
        cout << "Author: " << best_book.author_name << endl;
        cout << "Average Rating: " << setprecision(2) << fixed << max_book_ar << endl;
    } catch (exception &e) {
        cout << e.what() << endl;
    }

    return 0;
}