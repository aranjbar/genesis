#ifndef GENESIS_TYPES_HPP
#define GENESIS_TYPES_HPP

#include <cstdint>
#include <string>

typedef struct book_t book_t;
typedef struct review_t review_t;

struct book_t {
    uint32_t id;
    std::string title;
    std::string genre_1;
    std::string genre_2;
    uint32_t n_pages;
    std::string author_name;
    double author_ar;  // author average rating
};

struct review_t {
    uint32_t book_id;
    uint32_t rating;
    uint32_t n_likes;
};

#endif //GENESIS_TYPES_HPP