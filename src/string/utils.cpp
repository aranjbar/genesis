//
// Created by kali on 4/4/20.
//

#include <iostream>
#include <utility>
#include <cstdint>
#include "string/utils.hpp"
using namespace std;

// explode splits s into a slice of strings,
// one string per character up to a maximum of n (n < 0 means no limit).
vector<string> explode(string s, int n) {
    auto l = s.length();
    if (n < 0 || n > l) {
        n = l;
    }
    vector<string> a(n);
    for (auto i = 0; i < n-1; ++i) {
        a[i] = s[i];
        s = s.substr(i+1);
    }
    if (n > 0) {
        a[n - 1] = s;
    }
    return a;
}

// index returns the index of the first instance of substr in s, or -1 if substr is not present in s.
auto index(const string& s, const string& substr) -> int {
    auto found = s.find(substr);
    if (found == string::npos) {
        return -1;
    }
    return found;
}

// count counts the number of non-overlapping instance of substr in s.
// If substr is an empty string, count returns 1 + the number of points in s.
auto count(string s, const string& substr) -> int {
    if (substr.empty()) {
        return (int) s.size() + 1;
    }

    auto n = 0;
    while (true) {
        auto i = index(s, substr);
        if (i == -1) {
            return n;
        }
        ++n;
        s = s.substr(i+substr.size());
    }
}

// contains reports whether substr is within s.
bool contains(const string& s, const string& substr) {
    return index(s, substr) >= 0;
}

// generic split: splits after each instance of sep
vector<string> gen_split(string s, const string& sep, int n) {
    if (n == 0) {
        return vector<string>(0);
    }
    if (sep.empty()) {
        return explode(s, n);
    }
    if (n < 0) {
        n = count(s, sep) + 1;
    }

    vector<string> a(n);
    --n;
    auto i = 0;
    while (i < n) {
        auto m = index(s, sep);
        if (m < 0) {
            break;
        }
        a[i] = s.substr(0, m);
        s = s.substr(m+sep.size());
        ++i;
    }
    a[i] = s;
    return a;
}

vector<string> split(string s, const string& sep) {
    return gen_split(move(s), sep, -1);
}

// trim_space returns a slice of the string s, with all leading
// and trailing white space removed.
string trim_space(string s) {
    while (isspace(s[0])) {
        s.erase(0, 1);
    }

    while (isspace(s[s.size()-1])) {
        s.erase(s.size()-1, 1);
    }

    return s;
}

vector<string> split_trim_space(string s, const string& sep) {
    auto res = split(move(s), sep);
    for (auto & t : res) {
        t = trim_space(t);
    }
    return res;
}

// trim_left returns a substring of the string s with all trailing
// points contained in cut_set removed.
auto trim_left(string s, const string& cut_set) -> string {
    if (s.empty() || cut_set.empty()) {
        return s;
    }
    uint32_t i = 0;
    for (auto c : s) {
        if (contains(cut_set, string(1, c))) {
            i++;
        } else {
            break;
        }
    }
    return s.substr(i);
}

// trim_right returns a substring of the string s with all leading
// points contained in cut_set removed.
auto trim_right(string s, const string& cut_set) -> string {
    if (s.empty() || cut_set.empty()) {
        return s;
    }
    uint32_t i;
    for (i = s.size()-1; i > -1; --i) {
        if (!contains(s, string(1, s[i]))) {
            break;
        }
    }
    return s.substr(0, i+1);
}

// trim returns a substring of the string s with all leading and
// trailing points contained in cut_set removed.
auto trim(string s, const string& cut_set) -> string {
    return trim_right(trim_left(move(s), cut_set), cut_set);
}

auto split_trim(string s, const string& sep, const string& cut_set) -> vector<string> {
    auto res = split(move(s), sep);
    for (auto & t : res) {
        t = trim(t, cut_set);
    }
    return res;
}
