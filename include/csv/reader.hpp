//
// Created by kali on 4/25/20.
//

#ifndef CSV_READER_HPP
#define CSV_READER_HPP

#include <string>
#include <vector>
#include <cstdint>
#include <fstream>
#include <stdexcept>

#include "string/utils.hpp"

namespace CSV {
    template<uint32_t>
    class Reader {
    public:
        explicit Reader(const std::string &, char _delimiter = ',', const std::string &_trim_chars = " \t");

        template<class ...ColName>
        void read_header(ColName &...);

        template<class ...ColType>
        bool read_row(ColType &...);

    private:
        bool read_line();
        template<class ...ColName>
        void set_col_names(std::string s, ColName ...cols);
        void set_col_names() {}

        template<class T, class ...ColType>
        void parse_helper(std::size_t, T&, ColType&...);
        void parse_helper(std::size_t) {}

        std::ifstream in;
        char delimiter;
        std::string trim_chars;
        std::vector<std::string> col_names;
        std::vector<std::uint32_t> col_indices;
        std::vector<std::string> row;
    };


    template<uint32_t n_cols>
    Reader<n_cols>::Reader(const std::string &filename, const char _delimiter, const std::string &_trim_chars) {
        in = std::ifstream(filename);
        delimiter = _delimiter;
        trim_chars = _trim_chars;
        col_names = std::vector<std::string>(n_cols);
        col_indices = std::vector<uint32_t>(n_cols, -1);
    }

    template<uint32_t n_cols>
    template<class ...ColName>
    void Reader<n_cols>::set_col_names(std::string s, ColName ...cols) {
        col_names[n_cols - sizeof...(ColName) - 1] = std::move(s);
        set_col_names(std::forward<ColName>(cols)...);
    }

    template<uint32_t n_cols>
    bool Reader<n_cols>::read_line() {
        std::string line;
        getline(in, line);
        if (line.empty()) {
            return false;
        }
        row = split_trim(line, std::string(1, delimiter), trim_chars);
        return true;
    }

    template<uint32_t n_cols>
    template<class ...ColName>
    void Reader<n_cols>::read_header(ColName &...cols) {
        if (sizeof...(ColName) < n_cols) {
            throw std::invalid_argument("not enough column names specified");
        }
        if (sizeof...(ColName) > n_cols) {
            throw std::invalid_argument("too many column names specified");
        }

        set_col_names(std::forward<ColName>(cols)...);

        if (!read_line()) {
            throw std::invalid_argument("empty file");
        }
        for (int i = 0; i < col_names.size(); ++i) {
            auto it = find(row.begin(), row.end(), col_names[i]);
            if (it == row.end()) {
                col_names.erase(col_names.begin() + i);
                col_indices.erase(col_indices.begin() + i);
            } else {
                col_indices[i] = (uint32_t) distance(row.begin(), it);
            }
        }
    }

    void parse(const std::string& col, double & x) {
        x = std::stod(col, nullptr);
    }

    void parse(const std::string& col, uint32_t& x) {
        x = std::stoul(col, nullptr);
    }

    void parse(const std::string& col, std::string& x) {
        x = col;
    }

    template<uint32_t n_cols>
    template<class T, class ...ColType>
    void Reader<n_cols>::parse_helper(std::size_t r, T& t, ColType& ...cols) {
        parse(row[r], t);
        parse_helper(r+1, cols...);
    }

    template<uint32_t n_cols>
    template<class ...ColType>
    bool Reader<n_cols>::read_row(ColType &...cols) {
        if (sizeof...(ColType) < n_cols) {
            throw std::invalid_argument("not enough column names specified");
        }
        if (sizeof...(ColType) > n_cols) {
            throw std::invalid_argument("too many column names specified");
        }

        if (!read_line()) {
            return false;
        }
        parse_helper(0, cols...);
        return true;
    }
}

#endif //CSV_READER_HPP
