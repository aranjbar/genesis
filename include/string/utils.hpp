//
// Created by kali on 4/2/20.
//

#ifndef STRING_UTILS_H
#define STRING_UTILS_H

#include <algorithm>
#include <vector>
#include <string>
#include <cctype>

std::vector<std::string> explode(std::string, int);

auto index(const std::string&, const std::string&) -> int;

auto count(std::string, const std::string&) -> int;

bool contains(const std::string&, const std::string&);

std::vector<std::string> gen_split(std::string, const std::string&, int);

std::vector<std::string> split(std::string, const std::string&);

std::string trim_space(std::string);

std::vector<std::string> split_trim_space(std::string s, const std::string& sep);

auto trim_left(std::string, const std::string&) -> std::string;

auto trim_right(std::string, const std::string&) -> std::string;

auto trim(std::string, const std::string&) -> std::string;

auto split_trim(std::string, const std::string&, const std::string&) -> std::vector<std::string>;

#endif //STRING_UTILS_H
